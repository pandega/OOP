<?php 
require("animal.php");
require("frog.php");
require("ape.php");

$sheep = new Animal("shaun");

echo "Nama : $sheep->name <br>";
echo "Jumlah : $sheep->legs <br>";
echo "Tipe : $sheep->cold_blooded <br><br>";

$kodok = new Frog("Buduk");
echo "Nama : $kodok->name <br>";
echo "Jumlah : $kodok->legs <br>";
echo "Tipe : $kodok->cold_blooded <br>";
echo $kodok->jump();
echo "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Nama : $sungokong->name <br>";
echo "Jumlah : $sungokong->legs <br>";
echo "Tipe : $sungokong->cold_blooded <br>";
echo $sungokong->yell();

 ?>